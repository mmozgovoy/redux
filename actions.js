/*
 * action types
 */
 
export const ADD_ITEM = 'ADD_ITEM'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const TOGGLE_ITEM = 'TOGGLE_ITEM'
 
/*
 * action creators
 */
 
export function addItem(text) {
  return { type: ADD_ITEM, text }
}

export function removeItem(){
  return {type : REMOVE_ITEM}
}
 
export function toggleItem(index) {
  return { type: TOGGLE_ITEM, index }
}
