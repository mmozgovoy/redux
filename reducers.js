import { combineReducers } from 'redux'
import {
  ADD_ITEM,
  REMOVE_ITEM,
  TOGGLE_ITEM,
} from './actions'
 
function items(state = [], action) {
  switch (action.type) {
    case ADD_ITEM:
      return [
        ...state,
        {
          text: action.text,
          completed: false
        }
      ]
    case TOGGLE_ITEM:
      return state.map((item, index) => {
        if (index === action.index) {
          return Object.assign({}, item, {
            completed: !item.completed
          })
        }
        return item
      })
    case REMOVE_ITEM:
      if ([...state].length === 0) {
        return []
      }
      else if ([...state].length === 1) {
        return []
      }
      else return [...state].slice(0,[...state].length-1)

    default:
      return state
  }
}
 
const itemApp = combineReducers({
  items
})
 
export default itemApp
