import React from 'react'
import AddItem from '../containers/AddItem'
import RemoveItem from '../containers/RemoveItem'
import VisibleItemList from '../containers/VisibleItemList'
 
const App = () => (
  <div>
    <AddItem />
    <RemoveItem />
    <VisibleItemList />
  </div>
)
 
export default App
