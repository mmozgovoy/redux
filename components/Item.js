import React from 'react'
import PropTypes from 'prop-types'
 
const Item = ({ onClick, completed, text }) => (
  <li
    onClick={onClick}
    style={ {
      color: completed ? 'green' : 'red'
    }}
  >
    {text}
  </li>
)
 
Item.propTypes = {
  onClick: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
}
 
export default Item
