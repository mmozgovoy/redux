import React from 'react'
import PropTypes from 'prop-types'
import Item from './Item'
 
const ItemList = ({ items, onItemClick }) => (
  <ul>
    {items.map((item, index) => (
      <Item key={index} {...item} onClick={() => onItemClick(index)} />
    ))}
  </ul>
)
 
ItemList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onItemClick: PropTypes.func.isRequired
}
 
export default ItemList
