import React from 'react'
import { connect } from 'react-redux'
import { removeItem } from '../actions'
 
let RemoveItem = ({ dispatch }) => {

  let input
 
  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault();

          dispatch(removeItem())
        }}
      >
        <button type="submit">
          Remove Last Item
        </button>
      </form>
    </div>
  )
}
RemoveItem = connect()(RemoveItem)
 
export default RemoveItem
