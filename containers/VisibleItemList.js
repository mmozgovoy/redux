import { connect } from 'react-redux'
import { toggleItem } from '../actions'
import ItemList from '../components/ItemList'
 
const getItems = (items) => {
      return items
}
 
const mapStateToProps = state => {
  return {
    items: getItems(state.items)
  }
}
 
const mapDispatchToProps = dispatch => {
  return {
    onItemClick: id => {
      dispatch(toggleItem(id))
    }
  }
}
 
const VisibleItemList = connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemList)
 
export default VisibleItemList
